﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FramePerSecond : MonoBehaviour
{

    [SerializeField] Text _TargetText = null;

    void Awake()
    {
        StartCoroutine(CO_CheckingFPS());
    }

    IEnumerator CO_CheckingFPS()
    {
        float _DeltaTime = 0;

        while (true)
        {
            _DeltaTime += (Time.unscaledDeltaTime - _DeltaTime) * 0.1f;

            float _Msec = _DeltaTime * 1000.0f;
            float _Fps = 1.0f / _DeltaTime;
           
                if (_TargetText != null)
                    _TargetText.text = string.Format("{0:0.0}ms FPS:{1:0.}", _Msec, _Fps);


            yield return 0;
        }


    }


}
