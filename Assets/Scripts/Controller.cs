﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : UpdateManager
{

    private Vector3 _MoveVector3_Z = Vector3.forward;
    private Vector3 _MoveVector3_X = Vector3.right;

    private float _PrvAxisX = 0;
    private float _NxtAxisX = 0;

    private float _PrvAxisY = 0;
    private float _NxtAxisY = 0;

    private Vector3 _Velocity = Vector3.zero;

    public void Initialize()
    {

    }

    public void Update()
    {
        InputKeyboard();
        InputMouse();
    }


    private void InputKeyboard()
    {
        Vector3 _MoveVector3 = Vector3.zero;

        _MoveVector3_Z = Vector3.zero;
        _MoveVector3_X = Vector3.zero;

        bool _AxisZ = false;
        bool _AxisX = false;
        bool _isMovig = false;
        float _AnimSpeed = 1;

        if (Input.GetKey(KeyCode.W))
        {
            _MoveVector3_Z = Player.Get.transform.forward;
            _AxisZ = true;
            _isMovig = true;
        }

        if(Input.GetKey(KeyCode.S))
        {
            _MoveVector3_Z = -Player.Get.transform.forward;
            _AxisZ = true;
            _isMovig = true;
            _AnimSpeed = -1;
        }

        if (Input.GetKey(KeyCode.A))
        {
            _MoveVector3_X = -Player.Get.transform.right;
            _AxisX = true;
            _isMovig = true;
            Player.Get._Anim.SetInteger(AnimatorKey.LeftRight, -1);
        }

        if (Input.GetKey(KeyCode.D))
        {
            _MoveVector3_X = Player.Get.transform.right;
            _AxisX = true;
            _isMovig = true;
            Player.Get._Anim.SetInteger(AnimatorKey.LeftRight, 1);
        }

        if(Input.GetKeyDown(KeyCode.LeftShift))
        {
            Player.Get._AttackSystem.AttackState();
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            Player.Get._AttackSystem.SpecialAtttack();
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            Player.Get._AttackSystem.KickAttack();
        }

        _MoveVector3 = _MoveVector3_Z + _MoveVector3_X;

        if (_AxisZ && _AxisX)
            _MoveVector3 *= Mathf.Sqrt(0.5f);

        _Velocity = (_MoveVector3 * Player.Get._MoveSpeed);

        if (Player.Get._isMoveOn == false)
        {
            Player.Get._Anim.SetBool(AnimatorKey.Moving, false);
            Player.Get._Anim.SetFloat(AnimatorKey.AnimationSpeed, 1);
            return;
        }


        Player.Get._Anim.SetBool(AnimatorKey.Moving, _isMovig);
        Player.Get._Anim.SetFloat(AnimatorKey.AnimationSpeed, _AnimSpeed);

        Player.Get._Anim.SetFloat(AnimatorKey.VelocityZ, _Velocity.magnitude * 3);

        Player.Get._Rig.MovePosition(Player.Get.transform.localPosition + _Velocity * Time.deltaTime);

    }


    private void InputMouse()
    {
        float _AxisVelocity_X = 0;
        float _AxisVelocity_Y = 0;

        _NxtAxisX += Input.GetAxis(AnimatorKey.MouseX);
        _AxisVelocity_X = (_NxtAxisX - _PrvAxisX) * Player.Get._AxisSensitivity;
        _PrvAxisX = _NxtAxisX;

        _NxtAxisY += Input.GetAxis(AnimatorKey.MouseY);
        _AxisVelocity_Y = (_NxtAxisY - _PrvAxisY) * Player.Get._AxisSensitivity;
        _PrvAxisY = _NxtAxisX;

        
        Vector3 _TargetVector = Player.Get.transform.position + Player.Get.transform.forward * 3 + Player.Get.transform.right * _AxisVelocity_X;

        Player.Get.transform.LookAt(_TargetVector);

        if (Input.GetMouseButtonDown(0))
        {
            Player.Get._AttackSystem.NormalAttack();
        }



    }

    public bool OnGizmos = false;
    public void OnDrawGizmos()
    {
        if (!OnGizmos) return;

        Vector3 _MoveVector3 = Vector3.zero;
        _MoveVector3 = _MoveVector3_Z + _MoveVector3_X;

        Gizmos.color = Color.red;
        Vector3 _From = Player.Get.transform.position + Vector3.up * 1.5f;
        Vector3 _To = _From + _MoveVector3 * 2.5f;

        Gizmos.DrawLine(_From, _To);
    }

}


public class AnimatorKey
{
    public const string Moving = "Moving";
    public const string LeftRight = "LeftRight";
    public const string AnimationSpeed = "AnimationSpeed";

    public const string Weapon = "Weapon";
    public const string Action = "Action";

    public const string MouseX = "Mouse X";
    public const string MouseY = "Mouse Y";

    public const string VelocityZ = "Velocity Z";
    public const string VelocityX = "Velocity X";

    public const string InstantSwitchTrigger = "InstantSwitchTrigger";

    public const string WeaponUnsheathTrigger = "WeaponUnsheathTrigger";
    public const string WeaponSheathTrigger = "WeaponSheathTrigger";

    public const string AttackTrigger = "AttackTrigger";
    public const string AttackKickTrigger = "AttackKickTrigger";
    public const string SpecialAttackTrigger = "SpecialAttackTrigger";
    public const string SpecialEndTrigger = "SpecialEndTrigger";
    
}
