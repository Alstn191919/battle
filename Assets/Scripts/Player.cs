﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Controller _Controllger = new Controller();
    public AttackSystem _AttackSystem = new AttackSystem();

    public Animator _Anim;
    public Rigidbody _Rig;
    public Camera _Camera;
    public Weapon _Weapon;

    public float HP = 100;
    public float MP = 100;
    public float _MoveSpeed = 1;
    public float _AxisSensitivity = 1f;
    public int _Level = 1;

    public bool _isMoveOn = true;

    public static Player Get;

    void Awake()
    {
        _Anim = GetComponent<Animator>();
        _Rig = GetComponent<Rigidbody>();

        if (_Weapon == null)
            _Weapon = GameObject.Find("WeaponObj").gameObject.transform.GetChild(0).GetComponent<Weapon>();

        _Camera = Camera.main;
        Get = this;

        _AttackSystem.Initialize();
        _Controllger.Initialize();
        _Controllger.OnGizmos = true;
    }


    void FixedUpdate()
    {
        _Controllger.Update();
        _AttackSystem.Update();

    }


    private void OnDestroy()
    {

    }


    private void OnDrawGizmos()
    {
        _Controllger.OnDrawGizmos();
    }

}

public interface UpdateManager
{
     void Initialize();
     void Update();
}
    
