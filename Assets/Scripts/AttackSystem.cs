﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackSystem : UpdateManager
{

    float _AttackDelay = 1.2f;
    int _AttackIndex = 1;
    int _AttackKickIndex = 1;

    bool _isAttackOn = true;
    bool _SpecialOn = false;

    public void Initialize()
    {
        _isAttackOn = true;
    }

    public void Update()
    {
        
    }

    public void NormalAttack()
    {
        if (_isAttackOn == false) return;

        if (Player.Get._Anim.GetInteger(AnimatorKey.Weapon) == 1)
        {
            if(Player.Get._Anim.GetBool(AnimatorKey.Moving))
                _AttackIndex = 1;

            Player.Get._Weapon.GetComponent<BoxCollider>().enabled = true;
            Player.Get._Anim.SetInteger(AnimatorKey.Action, _AttackIndex);
            Player.Get._Anim.SetTrigger(AnimatorKey.AttackTrigger);

            Player.Get._isMoveOn = false;
            _isAttackOn = false;
            _AttackIndex++;

            if (_AttackIndex > 10)
                _AttackIndex = 1;

            Player.Get.StartCoroutine(CO_AttackDelay(_AttackDelay));
        }
           
    }

    public void KickAttack()
    {
        if (_isAttackOn == false) return;

        if (Player.Get._Anim.GetInteger(AnimatorKey.Weapon) == 1)
        {
            if (Player.Get._Anim.GetBool(AnimatorKey.Moving))
                _AttackKickIndex = 1;

            Player.Get._Anim.SetInteger(AnimatorKey.Action, _AttackKickIndex);
            Player.Get._Anim.SetTrigger(AnimatorKey.AttackKickTrigger);

            Player.Get._isMoveOn = false;
            _isAttackOn = false;
            _AttackKickIndex++;

            if (_AttackKickIndex > 4)
                _AttackKickIndex = 1;

            Player.Get.StartCoroutine(CO_AttackDelay(_AttackDelay));
        }
    }

    public void SpecialAtttack()
    {
        if(_SpecialOn == false)
        {
            _SpecialOn = true;
            Player.Get._Anim.SetTrigger(AnimatorKey.SpecialAttackTrigger);
        }
        else
        {
            _SpecialOn = false;
            Player.Get._Anim.SetTrigger(AnimatorKey.SpecialEndTrigger);
        }

    }

    IEnumerator CO_AttackDelay(float _Delay)
    {
        float _DelayTime = 0;
        float _CoolTime = _Delay;
        float _AttackTime = _Delay * 0.8f;
        while (true)
        {
            _DelayTime += Time.unscaledDeltaTime;

            if (_DelayTime >= _AttackTime)
                Player.Get._Weapon.GetComponent<BoxCollider>().enabled = false;

            if (_DelayTime >= _CoolTime)
                break;

            yield return 0;
        }

        _isAttackOn = true;
        Player.Get._isMoveOn = true;
    }

    public void AttackState()
    {
        if (Player.Get._Anim.GetInteger(AnimatorKey.Weapon) == 1)
        {
            Player.Get._Anim.SetInteger(AnimatorKey.Weapon, -1);
            Player.Get._Anim.SetInteger(AnimatorKey.Action, 0);
            Player.Get._Anim.SetTrigger(AnimatorKey.InstantSwitchTrigger);
            Player.Get._Weapon.gameObject.SetActive(false);
        }
        else
        {
            Player.Get._Anim.SetInteger(AnimatorKey.Weapon, 1);
            Player.Get._Anim.SetInteger(AnimatorKey.Action, 1);
            Player.Get._Anim.SetTrigger(AnimatorKey.WeaponSheathTrigger);
            Player.Get._Weapon.gameObject.SetActive(true);
        }
    }
}
